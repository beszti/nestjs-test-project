# NestJS test project

NestJS test project with postman.
You can insert, delete, update products. First at all you can upload automatically the array if you need (more below).

## Getting started

```
git clone [this project]
cd nest-example
npm install
node run start
```
Open the postman app and load the endpoints on localhost:3000 as you wish.

## Endpoints
POST products/setfirst - it upload the array with 6 element (only first step OR the array is empty yet)<br />
GET products - get all inserted products<br />
GET products/:id - add an id and get the product<br />
DELETE products/:id - add an id and remove the product from the array<br />
PATCH products/:id - add an id and update the product in the array<br />
POST products/insert - in the body/raw, type:json give a json example:<br />{"name": "Peach", "price": 2.0, "description": "fruit"}

